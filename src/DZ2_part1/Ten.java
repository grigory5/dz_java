package DZ2_part1;
import java.util.Scanner;

public class Ten {
    public static void start_game(){
        Scanner input = new Scanner(System.in);
        int M = (int)(Math.random()*1000);
        int inp; // Введенное пользователем число

        System.out.println("Введите число в диапазоне от 0 до 1000\n" +
                "Чтобы выйти из игры введите отрицательное число");
        while (true){
            inp = input.nextInt();
            if(inp < 0) break;
            if(M == inp) {
                System.out.println("Победа !");
                break;
            } else if (M > inp) {
                System.out.println("Это число меньше загаданного");
            } else System.out.println("Это число больше загаданного");

        }
    }

    public static void main(String[] args) {
        start_game();
    }
}

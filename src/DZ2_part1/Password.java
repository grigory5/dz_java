package DZ2_part1;
import java.util.Random;
import java.util.Scanner;

public class Password {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество символов пароля");
        int N;
        do {
            N = input.nextInt();
            if(N < 8)  System.out.print("Пароль с "+ N + " количеством символов небезопасен.\nВведите пароль еще раз.\n");
        } while (N < 8);

        /*
        65 - 90: Диапазон заглавных символов
        97 - 122: Диапазон срочных символов
        48 - 57: Диапазон цифровых символов
        42 45 95: Номера специальных символов
         */

        int[] x = new int[4]; // Массив в котором имеетс: Количество заглавых символв, Количество строчных симвовлов и тд
        x[0] = (int)(Math.random()*(N - 4) + 1 + 0.5);
        x[1] = (int)(Math.random()*(N - 3 - x[0]) + 1 + 0.5);
        x[2] = (int)(Math.random()*(N - 2 - x[0] - x[1]) + 1 + 0.5);
        x[3] = (int)(Math.random()*(N - 1 - x[0] - x[1] - x[2]) + 1 + 0.5);
        /* Генерирует случайное количество строчных, прописных, нумерных и специальных символов */

        if(x[0] + x[1] + x[2] + x[3] < N){
            int i = (int)(Math.random()*(1000)) % 4;
            x[i] +=1;
        }
        /* Если количество символов меньше */

        char[] password = new char[N];

        int k = 0; // вспомогательная переменная
        for(int i = 0; i < x.length; i++){
            for(int j = 0; j < x[i]; j++){
                if(i == 0){ // Генерирует Диапазон заглавных символов
                    password[k] =(char)( (int)(Math.random()*25 + 65) );
                }
                if(i == 1){ // Генерирует Диапазон срочных символов
                    password[k] =(char)( (int)(Math.random()*25 + 97) );
                }
                if(i == 2){ // Генерирует Диапазон нумерных символов
                    password[k] =(char)( (int)(Math.random()*9 + 48) );
                }
                if(i == 3){ // Генерирует специальные символы
                    int g = (int)(Math.random()*(1000)) % 3;
                    if(g == 0) password[k] ='-';
                    else if(g == 1) password[k] ='_';
                    else  password[k] ='*';
                }
                k++;
            }
        }

        char tmp;
        for(int i = 0; i < N; i++){ // перемешиваем пароль случайным образом
            k = (int)(Math.random()*(N));
            if(k == N) k -= 1;
            tmp = password[i];
            password[i] = password[k];
            password[k] = tmp;
        }

        //Вывод сгенерированного пароля
        System.out.println(password);
    }
}

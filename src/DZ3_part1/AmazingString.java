package DZ3_part1;

public class AmazingString {
    char[] massString;

    AmazingString(char[] massChar){
        this.massString = massChar;
    }

    AmazingString(String str){
        this.massString = new char[str.length()];
        for (int i = 0; i < str.length(); i++){
            massString[i] = str.charAt(i);
        }
    }

    public char chAt(int i){
        return this.massString[i];
    }

    public int sizeOfStr(){
        return this.massString.length;
    }

    public void printStr(){
        for (int i = 0; i < this.massString.length - 1; i++){
            System.out.print(this.massString[i]);
        }
        System.out.println(this.massString[-1]);
    }

    public boolean isSubCh(char[] sub){
        int flag = sub.length;
        int is = 0;
        for(int i = 0; i < this.massString.length; i++){
            if(this.massString[i] == sub[is]){
                is++;
                flag -= 1;
            }
            else {
                flag = sub.length;
                is = 0;
            }
        }
        if(flag == 0) return true;
        else return false;
    }

    public boolean isSubStr(String sub){
        int flag = sub.length();
        int is = 0;
        for(int i = 0; i < this.massString.length; i++){
            if(this.massString[i] == sub.charAt(is)){
                is++;
                flag -= 1;
            }
            else {
                flag = sub.length();
                is = 0;
            }
        }
        if(flag == 0) return true;
        else return false;
    }

    public void delTub(){
        int count = 0;
        for (int i = 0; i < this.massString.length; i++){
            if(this.massString[i] == ' ') count++;
        }

        char[] new_mass = new char[this.massString.length - count];
        for (int i = 0, j = 0; i < this.massString.length; i++){
            if(this.massString[i] != ' '){
                new_mass[j] = massString[i];
                j++;
            }
        }
        this.massString = new_mass;
    }

    public void rev(){
        char buf;
        for(int i = 0; i < this.massString.length/2; i++){
            buf = this.massString[(-1 - i)];
            this.massString[(-1 - i)] = this.massString[i];
            this.massString[i] = buf;
        }
    }
}

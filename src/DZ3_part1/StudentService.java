package DZ3_part1;

public class StudentService {
    public Student bestStudent(Student[] students){
        double max = students[0].getAverage();
        int maxi = 0;
        for (int i = 1; i < students.length; i++){
            if(students[i].getAverage() > max){
                max = students[i].getAverage();
                maxi = i;
            }
        }
        return students[maxi];
    }

    public void sortBySurname(Student[] students){
        Student buf;
        for(int i = 0; i < students.length - 1; i++) {
            for(int j = 0; j < students.length - i - 1; j++) {
                if(students[j].getSurname().compareTo(students[j + 1].getSurname()) < 0 ){
                    buf = students[j + 1];
                    students[j + 1] = students[j];
                    students[j] = buf;
                }
            }
        }
    }
}

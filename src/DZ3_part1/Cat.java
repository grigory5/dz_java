package DZ3_part1;

public class Cat {
    private static void sleep(){
        System.out.println("Sleep");
    }

    private static void meow(){
        System.out.println("Meow");
    }

    private static void eat(){
        System.out.println("Eat");
    }

    public static void status(){
        int i = (int)(Math.random()*1000) % 3;
        if(i == 0) sleep();
        if(i == 1) meow();
        if (i == 2) eat();
    }

    public static void main(String[] args) {
        for (int i  = 0; i < 15; i++) status();
    }
}

package DZ3_part1;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    TimeUnit(int hours, int minutes, int seconds){
        if(0 <= hours && hours <= 23)
            this.hours = hours;
        if(0 <= minutes && minutes <= 59)
            this.minutes = minutes;
        if(0 <= seconds && seconds <= 59)
            this.seconds = seconds;
    }
    TimeUnit(int hours, int minutes){
        if(0 <= hours && hours <= 23)
            this.hours = hours;
        if(0 <= minutes && minutes <= 59)
            this.minutes = minutes;
        this.seconds = 0;
    }
    TimeUnit(int hours){
        if(0 <= hours && hours <= 23)
            this.hours = hours;
        this.minutes = 0;
        this.seconds = 0;
    }

    public void printTime(){
        System.out.println(this.hours + ":" + this.minutes + ":" + this.seconds);
    }

    public void printTimeAMPM(){
        System.out.print(this.hours + ":" + this.minutes + ":" + this.seconds);
        System.out.println(this.hours > 12 ? " pm" : " am");
    }

    public void addTime(int hours, int minutes, int seconds){
        this.hours += hours;
        if(this.hours > 23){
            this.hours = this.hours % 24;
        }
        this.minutes += minutes;
        if(this.minutes > 60){
            this.hours += this.minutes/60;
            this.hours = this.hours % 24;
            this.minutes = this.minutes % 60;
        }
        this.seconds += seconds;
        if(this.seconds > 60){
            this.hours += (this.seconds/60)/60;
            this.hours = this.hours % 24;
            this.minutes += this.seconds/60;
            this.minutes = this.minutes % 60;
        }
    }
}

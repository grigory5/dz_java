package DZ3_part1;

public class Atm {
    int rub;
    int doll;
    double course;

    static int count = 0;
    Atm(double course){
        this.course = course;
        count++;
    }

    void RubToDoll(){
        this.rub = (int) (this.doll * this.course);
    }

    void DollToRub(){
        this.doll = (int) (this.rub / this.course);
    }

    int getCount(){
        return count;
    }
}

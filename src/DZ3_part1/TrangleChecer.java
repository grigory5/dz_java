package DZ3_part1;

public class TrangleChecer {
    public static boolean check(double a, double b, double c){
        if(a >= b && a >= c){
            return (a < b + c);
        } else if(b >= a && b >= c){
            return (b < a + c);
        }
        else if(c >= a && c >= b){
            return (c < a + b);
        }
        else return true;
    }
}

package DZ3_part1;

public class Student {
    private String name;
    private String surname;
    private int[] grades;

    Student(){

    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getSurname(){
        return this.surname;
    }
    public void setSurname(String name){
        this.surname = surname;
    }

    public int[] getGrades(){
        return this.grades;
    }
    public void setGrades(int[] grades){
        this.grades = grades;
    }

    public void addGrade(int gr){
        int[] newGrades;
        if(this.grades.length < 10){
            newGrades = new int[this.grades.length + 1];
            for(int i = 0; i < newGrades.length; i++){
                newGrades[-1] = gr;
            }
        }
        else {
            newGrades = new int[10];
            for (int i = 1; i < this.grades.length - 1; i++){
                newGrades[i - 1] = this.grades[i];
            }
            newGrades[9] = gr;
        }
    }

    public double getAverage(){
        int res = 0;
        for(int i = 0; i < this.grades.length; i++){
            res += this.grades[i];
        }
        return res/this.grades.length;
    }
}